import React, { useState } from "react";
import Icon from "./icon/Icon";
import { showModal } from "./atoms";
import { useRecoilValue, useRecoilState } from "recoil";
import OpeningHours from "./OpeningHours";
import Link from "next/link";
import { motion } from "framer-motion";

type Props = {};

const menuItems = [
  {
    name: "Menu",
    path: "/menu",
    slug: "menu",
  },
  {
    name: "Restaurant",
    path: "/restaurant",
    slug: "restaurant",
  },
  {
    name: "Classes",
    path: "/workshop",
    slug: "classes",
  },
];

const Menu = (props: Props) => {
  const [selected, setSelected] = useState<string>("");
  const [select, setTodoList] = useRecoilState(showModal);
  // const [showMenu, setShowMenu] = useState(false);

  return (
    <>
      <nav className="relative p-[10px] bg-white rounded-full font-medium text-[#081212] ">
        <ul className="flex  space-x-[2px] font-Chillax-medium">
          <li
            className="px-4 py-2 flex items-center"
            onMouseOver={() => setTodoList(true)}
            onMouseLeave={() => setTodoList(false)}
          >
            <Icon name="Clock" />
          </li>
          {menuItems.map((item) => {
            const { name, path, slug } = item;
            return (
              <Link key={slug} href={path}>
                <li
                  className={`${
                    selected === slug ? "bg-gray-100" : ""
                  } px-4 py-2 align-middle   flex items-center hover:bg-[#F8F8F8] rounded-full cursor-pointer `}
                  onClick={() => setSelected(slug)}
                >
                  {name}
                </li>
              </Link>
            );
          })}
          <Link href={"/reservation"}>
            <li
              className={`bg-black  px-4 py-2 flex items-center text-white rounded-full align-middle cursor-pointer`}
              onClick={() => {
                setSelected("book-a-table");
              }}
            >
              BOOK A TABLE
            </li>
          </Link>
        </ul>
        {/* <div className="animation absolute h-full start-home animation start-home"></div> */}
        {select && (
          <motion.div

          variants={{
            open: {
              clipPath: "inset(0% 0% 0% 0% round 10px)",
              transition: {
                type: "spring",
                bounce: 0,
                duration: 0.7,
                delayChildren: 0.3,
                staggerChildren: 0.05
              }
            },
            closed: {
              clipPath: "inset(10% 50% 90% 50% round 10px)",
              transition: {
                type: "spring",
                bounce: 0,
                duration: 0.3
              }
            }
          }}
            initial={"closed"}
            animate={"open"}
            className="absolute -top-44 "
          >
            <OpeningHours />
          </motion.div>
        )}
      </nav>
    </>
  );
};

export default Menu;
