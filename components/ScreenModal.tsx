import React, { JSXElementConstructor } from "react";
import { motion, AnimatePresence } from "framer-motion";
import { showModal } from "../components/atoms";
import { useRecoilValue, useRecoilState } from "recoil";

type Props = {
  children: JSX.Element;
};

const ScreenModal: React.FC<Props> = ({ children }) => {
  const backDropVariants = {
    open: { opacity: 1 },
    closed: { opacity: 0 },
  };

  const [showOpen, setShowOpen] = useRecoilState(showModal);

  return (
    <AnimatePresence exitBeforeEnter>
      {showOpen && (
        <motion.div
          variants={backDropVariants}
          initial="hidden"
          animate="visible"
          exit={{ opacity: 0 }}
          className="fixed top-0 left-0 z-10 bg-black bg-opacity-60 w-[100vw] h-[100vh] flex items-center justify-center "
          onClick={() => setShowOpen(false)}
        >
          <div
            className="flex items-center justify-center flex-col w-[560px]  rounded-xl p-10  bg-black shadow shadow-slate-700"
            onClick={(e) => e.stopPropagation()}
          >
            {children}
          </div>
        </motion.div>
      )}
    </AnimatePresence>
  );
};

export default ScreenModal;
