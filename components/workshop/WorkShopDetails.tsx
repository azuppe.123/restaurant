import React from "react";
import MenuGroup from "../menucomponents/MenuGroup";
import Details from "./Details"

type Props = {};

const items = {
  title: "Starters",
  id:"starter",
  items: [
    {
      id: 1,
      name: "Tomato Toast",
      vegan: true,
      img: "",
      special: null,
      description:
        "Bacon ipsum dolor amet beef corned beef meatloaf chislic flank, venison leberkas buffalo sirloin turducken meatball. Tail swine biltong kielbasa spare ribs, rump shank shankle.",
      price: null,
    },
    {
      id: 2,
      name: "Eggplant Toast",
      vegan: true,
      img: "",
      special: "Starter of the Day",
      description:
        "Bacon ipsum dolor amet beef corned beef meatloaf chislic flank, venison leberkas buffalo sirloin turducken meatball. Tail swine biltong kielbasa spare ribs, rump shank shankle.",
      price: null,
    },
    {
      id: 3,
      name: "Beef burger",
      vegan: false,
      special: null,
      img: "",
      description:
        "Bacon ipsum dolor amet beef corned beef meatloaf chislic flank, venison leberkas buffalo sirloin turducken meatball. Tail swine biltong kielbasa spare ribs, rump shank shankle.",
      price: null,
    },
  ],
};

const teachers = {
  title: "Teacher",
  id:"teacher",
  items: [
    {
      id: 3,
      name: "Beef burger",
      vegan: false,
      special: null,
      img: "",
      description:
        "Bacon ipsum dolor amet beef corned beef meatloaf chislic flank, venison leberkas buffalo sirloin turducken meatball. Tail swine biltong kielbasa spare ribs, rump shank shankle.",
      price: null,
    },
  ],
};

const WorkShopDetails = (props: Props) => {
  return (
    <div className="bg-black">
      <p className="font-Chillax text-base  leading-[160%]  text-white opacity-60 pb-2">
        Cooking classes
      </p>
      <h1 className="font-Chillax-medium text-[40px] leading-[100%] text-white pb-4">
      Reserve Your Spot
      </h1>
      <p className="font-Chillax text-[18px] leading-[160%]  text-white opacity-60 pb-8">
        Our dining atmosphere is casual and comfortable. To reflect this
        environment, we maintain a formal dress.
      </p>
      <div className="space-y-[80px]">
        <div className="flex gap-x-8 items-center">
          <div className="rounded-full text-center text-base leading-[100%] py-[22px] px-8 cursor-pointer text-[#081212] font-Chillax-medium uppercase bg-[#F8D49E]">
            Book a Spot
          </div>
          <p className=" font-Chillax-medium text-xl text-white">89€</p>
        </div>
        <Details/>
        <MenuGroup object={items} rounded={false} showSocial={false} />
        <MenuGroup object={teachers} rounded={true} showSocial={true} />
      </div>
    </div>
  );
};

export default WorkShopDetails;
