import Image from "next/image";
import React from "react";

const Details: React.FC = () => {
  return (
    <div className="space-y-8 w-full">
      <p className=" font-bitter-rose text-left text-primary text-[64px] leading-[70%] font-normal">
        Details
      </p>
      <div>
        <div className="grid grid-cols-1  gap-4">
          <div className=" divide-y-[1px]  divide-[#ffffff1a] space-y-4">
            <div className="flex justify-between">
              <p className=" font-Chillax-medium text-xl text-white">Date</p>
              <p className="font-Chillax text-base  leading-[160%]  text-white opacity-60 pb-2">
                June 16, 2023 8:00 PM
              </p>
            </div>
            <div className="flex justify-between pt-4">
              <p className=" font-Chillax-medium text-xl text-white">Teacher</p>
              <p className="flex gap-4">
                <div>
                  <Image
                    width={32}
                    height={32}
                    layout="fixed"
                    className=" object-fit rounded-full h-6 w-6"
                    src="/workshop.png"
                    alt=""
                  />
                </div>{" "}
                <p className="font-Chillax text-base  leading-[160%]  text-white opacity-60 ">
                  Jakob Grønberg
                </p>
              </p>
            </div>
            <div className="flex justify-between pt-4">
              <p className=" font-Chillax-medium text-xl text-white">
                Language
              </p>
              <p className="font-Chillax text-base  leading-[160%]  text-white opacity-60 pb-2">
                English
              </p>
            </div>
            <div className="flex justify-between pt-4">
              <p className=" font-Chillax-medium text-xl text-white">
                Location
              </p>
              <p className="font-Chillax text-base  leading-[160%]  text-white opacity-60 pb-2">
                <p>nique.</p>
                <p> Main Street 16 10629</p> <p>Berlin</p>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Details;
