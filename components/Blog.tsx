import type { NextPage } from "next";
import Icon from "../components/icon/Icon";
import Menu from "../components/Menu";
import OpeningHours from "../components/OpeningHours";
import Image from "next/image";
import Footer from "./Footer";

const items = [
  {
    id: 1,
    name: "Tomato Toast",
    img: "",
    date: "September 19, 2022",
    description:
      "Bacon ipsum dolor amet beef corned beef meatloaf chislic flank, venison leberkas buffalo sirloin turducken meatball. Tail swine biltong kielbasa spare ribs, rump shank shankle.",
  },
  {
    id: 2,
    name: "Eggplant Toast",
    date: "September 19, 2022",
    img: "",
    description:
      "Bacon ipsum dolor amet beef corned beef meatloaf chislic flank, venison leberkas buffalo sirloin turducken meatball. Tail swine biltong kielbasa spare ribs, rump shank shankle.",
  },
  {
    id: 3,
    name: "Beef burger",
    date: "September 19, 2022",
    img: "",
    description:
      "Bacon ipsum dolor amet beef corned beef meatloaf chislic flank, venison leberkas buffalo sirloin turducken meatball. Tail swine biltong kielbasa spare ribs, rump shank shankle.",
  },
  {
    id: 4,
    name: "Chicken burger",
    date: "September 19, 2022",
    img: "",
    description:
      "Bacon ipsum dolor amet beef corned beef meatloaf chislic flank, venison leberkas buffalo sirloin turducken meatball. Tail swine biltong kielbasa spare ribs, rump shank shankle.",
  },
];

const Blog: NextPage = () => {
  return (
    <div className="flex">
      <div className=" fixed h-[100vh] w-[50vw] -z-50">
        <Image
          layout="fill"
          className="w-[100vw] h-[100vh] object-cover"
          src="/blog.png"
          alt=""
        />
      </div>
      <section className="flex flex-col items-center justify-between p-16 h-[100vh] w-1/2 sticky top-0 left-0 ">
        <Icon name="Logo" />
        <div className="w-[685px]">
          <p className=" font-bitter-rose text-center text-primary text-[80px] leading-[90%] font-normal">
            Blog
          </p>
          <p className=" text-white font-Chillax-medium text-[80px] text-center leading-[90%] ">
            Latest News
          </p>
        </div>
        <Menu />
        <OpeningHours />
      </section>

      <section className="flex flex-col items-center justify-between p-16 space-y-12  bg-black">
        {items.map((xl, index) => {
          const { name, description, date } = xl;
          return (
            <>
              <div className="p-4 flex justify-between">
                <div className="flex gap-6 w-full">
                  <Image
                    src={"/pumpkin_soup.webp"}
                    layout="fixed"
                    width={280}
                    height={210}
                    alt="menu"
                    className={`rounded-lg
                        object-cover block`}
                  />

                  <div className="space-y-1">
                    <div className="flex justify-between w-full items-center ">
                      <div className="">
                        <p className="font-Chillax text-base text-[#FACE8D]">
                          {date}
                        </p>
                        <h3 className="font-Chillax-medium text-2xl text-white">
                          {name}
                        </h3>
                      </div>
                    </div>
                    <p className=" text-white opacity-60 font-Chillax text-base">
                      {description}
                    </p>
                  </div>
                </div>
              </div>
            </>
          );
        })}
      <Footer />
      </section>
    </div>
  );
};

export default Blog;
