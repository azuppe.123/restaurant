import React from "react";
import { motion } from "framer-motion";

type Props = {};

const OpeningHours = (props: Props) => {
  const itemVariants = {
    open: {
      opacity: 1,
      y: 0,
      transition: { type: "spring", stiffness: 300, damping: 24 },
    },
    closed: { opacity: 0, y: 20, transition: { duration: 0.2 } },
  };

  const openingHours = [
    { Mon: "closed" },
    { "Tue - Fri": "4pm - 8pm" },
    { "Sat- Sun": "5pm - 11pm" },
  ];

  return (
    <div className="py-4 px-6 bg-white rounded-xl font-medium text-[#081212] ">
      <p className="font-Chillax text-lg mb-2">Opening Hours</p>
      <div className="grid grid-cols-2 gap-x-10 gap-y-2">
        {openingHours.map((item, index) => (
          <>
            <motion.div
              variants={itemVariants}
              className="col-span-1 font-Chillax-medium"
            >
              {Object.keys(item)}
            </motion.div>
            <motion.div
              variants={itemVariants}
              className="col-span-1 font-Chillax place-self-end"
            >
              {Object.values(item)}
            </motion.div>
          </>
        ))}
      </div>
    </div>
  );
};

export default OpeningHours;
