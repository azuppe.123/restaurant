import type { NextPage } from "next";
import Icon from "./icon/Icon";
import Menu from "./Menu";
import Image from "next/image";
import { motion, Variants } from "framer-motion";

const Main: NextPage = () => {
  const imageVariants: Variants = {
    hidden: { opacity: 0, filter: "blur(10)" },
    visible: {
      filter: "blur(0)",
      opacity: 1,
      transition: {
        duration: 0.5,
      },
    },
  };

  const mainVariants: Variants = {
    hidden: { opacity: 0, x: "-10vw" },
    visible: {
      x: 0,
      opacity: 1,
      transition: {
        delay: 0.5,
        duration: 0.5,
        stiffness: 100,
        type: "spring",
      },
    },
  };

  const subVariants: Variants = {
    hidden: { opacity: 0, x: "10vw" },
    visible: {
      x: 0,
      opacity: 1,
      transition: {
        delay: 1,
        duration: 0.5,
        stiffness: 100,
        type: "spring",
      },
    },
  };

  const paraVariants: Variants = {
    hidden: { opacity: 0 },
    visible: {
      x: 0,
      opacity: 1,
      transition: {
        delay: 1.5,
        duration: 0.5,
      },
    },
  };

  const menuVariants: Variants = {
    hidden: { opacity: 0, y: "5vh" },
    visible: {
      y: 0,
      opacity: 1,
      transition: {
        delay: 2,
        duration: 0.5,
        type: "spring",
      },
    },
  };

  return (
    <div className="">
      <motion.div
        variants={imageVariants}
        initial={"hidden"}
        animate={"visible"}
        className=" fixed h-[100vh] w-[100vw] -z-50"
      >
        <Image
          layout="fill"
          className="w-[100vw] h-[100vh] object-cover"
          src="/main.png"
          alt=""
        />
      </motion.div>
      <div className="flex flex-col items-center justify-between p-16 h-[100vh]">
        <Icon name="Logo" />
        <div className="w-[685px]">
          <motion.p
            variants={mainVariants}
            initial="hidden"
            animate="visible"
            className=" font-bitter-rose text-center text-primary text-[80px] leading-[90%] font-normal"
          >
            The pure taste of
          </motion.p>
          <motion.p
            variants={subVariants}
            initial="hidden"
            animate="visible"
            className=" text-white font-Chillax-medium text-[160px] leading-[90%] "
          >
            Thailand
          </motion.p>
          <motion.p
            variants={paraVariants}
            initial="hidden"
            animate="visible"
            className="text-white font-normal  text-2xl leading-[160%] text-center pt-8"
          >
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore.
          </motion.p>
        </div>
        <motion.div variants={menuVariants} initial="hidden" animate="visible">
          <Menu />
        </motion.div>
      </div>
    </div>
  );
};

export default Main;
