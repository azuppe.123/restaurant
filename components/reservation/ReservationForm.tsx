import React from "react";

type Props = {};

const ReservationForm = (props: Props) => {
  return (
    <div className="bg-black">
      <h1 className="font-Chillax-medium text-[40px] leading-[100%] text-white">
        Book a table
      </h1>
      <p className="font-Chillax text-[20px] leading-[160%]  text-white opacity-60">
        Our dining atmosphere is casual and comfortable. To reflect this
        environment, we maintain a formal dress.
      </p>
      <div className="space-y-12">
        <div className="space-y-4">
          <p className="font-Chillax text-white text-opacity-80 text-base gap">
            Name
          </p>
          <input
            id="name"
            type="text"
            className="px-6 py-4 border rounded-[10px] bg-black border-white border-opacity-10 placeholder:font-Chillax  placeholder:text-white placeholder:text-lg"
            placeholder="Name"
          />
        </div>
        <div className="space-y-4">
          <p className="font-Chillax text-white text-opacity-80 text-base">
            Number of guests
          </p>
          <input
            id="Number of guests"
            type="text"
            className="px-6 py-4 border rounded-[10px] bg-black border-white border-opacity-10 placeholder:font-Chillax placeholder:text-white placeholder:text-lg"
            placeholder="2"
          />
        </div>
        <div className="flex gap-8">
          <div className="space-y-4">
            <p className="font-Chillax text-white text-opacity-80 text-base">
              Date
            </p>
            <input
              id="Date"
              type="text"
              className="px-6 py-4 border rounded-[10px] bg-black border-white border-opacity-10 placeholder:font-Chillax placeholder:text-white placeholder:text-lg"
              placeholder="19.08.22"
            />
          </div>
          <div className="space-y-4">
            <p className="font-Chillax text-white text-opacity-80 text-base">
              Time
            </p>
            <input
              id="name"
              type="text"
              placeholder="6:00pm"
              className="px-6 py-4 border rounded-[10px] bg-black border-white border-opacity-10 placeholder:font-Chillax placeholder:text-white placeholder:text-lg"
            />
          </div>
        </div>
        <div className="rounded-full w-full text-center text-base leading-[100%] py-[22px] cursor-pointer text-[#081212] font-Chillax-medium uppercase bg-[#F8D49E]">
          Book a table
        </div>
      </div>
    </div>
  );
};

export default ReservationForm;
