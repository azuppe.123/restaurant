import Link from "next/link";
import React from "react";
import Icon from "./icon/Icon";

type Props = {};
const pages = [
  {
    name: "Menu",
    href: "/menu",
  },
  {
    name: "Restaurant",
    href: "/restaurant",
  },
  {
    name: "Classes",
    href: "/workshop",
  },
  {
    name: "Book a Table",
    href: "/reservation",
  },
  {
    name: "Blog",
    href: "/blog",
  },
  {
    name: "Contact",
    href: "/",
  },
  {
    name: "Shop",
    href: "/",
  },
];

const utility = [
  "Styleguide",

  "Licensing",

  "Changelog",

  "404 Page",

  "Password Protected",
];

const Footer = (props: Props) => {
  return (
    <div className="grid grid-cols-3 gap-x-20">
      <div className="col-span-1 font-Chillax  text-white space-y-8">
        <Link href="/">
          <Icon name="Logo" />
        </Link>
        <p className=" text-[14px] opacity-60 ">
          By Pawel Gola Powered by Webflow
        </p>
      </div>
      <div className="col-span-1 font-Chillax text-lg text-white space-y-8">
        <p>Pages</p>
        <div className="text-base  space-y-4">
          {pages.map((item, index) => {
            const { name, href } = item;
            return (
              <Link key={index} href={href}>
                <p className="cursor-pointer opacity-60 hover:opacity-100 transition-all duration-75">
                  {name}
                </p>
              </Link>
            );
          })}
        </div>
      </div>
      <div className="col-span-1 font-Chillax text-lg text-white space-y-8">
        <p>Utility Pages</p>
        <div className="text-base  space-y-4">
          {utility.map((item, index) => {
            return (
              <p
                key={index}
                className="cursor-pointer opacity-60 hover:opacity-100 transition-all duration-75"
              >
                {item}
              </p>
            );
          })}
        </div>
      </div>
      <div className="col-span-1 font-Chillax text-lg text-white"></div>
    </div>
  );
};

export default Footer;
