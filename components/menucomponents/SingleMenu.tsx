import next from "next";
import React from "react";
import Image from "next/image";
import Icon from "../icon/Icon";
import { motion } from "framer-motion";

type Props = {
  rounded: Boolean;
  showSocial: Boolean;
  item: {
    id: any;
    name: string;
    vegan: any;
    img: string;
    special: any;
    description: string;
    price: Number | null;
  };
};

const SingleMenu: React.FC<Props> = ({ item, rounded, showSocial }) => {
  const { id, name, vegan, img, special, description, price } = item;
  return (
    <motion.div
      className={`rounded-xl p-4 flex justify-between ${
        special ? " border  border-primary" : null
      }`}
    >
      <div className={`flex gap-6 w-full  `}>
        <div className="w-[92px] h-[72px]">
          <Image
            src={"/pumpkin_soup.webp"}
            layout={"fixed"}
            width={92}
            height={72}
            alt="menu"
            className={`${
              rounded ? "rounded-full" : "rounded-lg"
            }  object-cover block`}
          />
        </div>

        <div className="space-y-1">
          <div className="flex justify-between w-full items-center ">
            <div className="flex gap-4 items-center">
              <h3 className="font-Chillax text-xl text-white">{name}</h3>
              {vegan ? <Icon name="leaf" /> : null}
            </div>

            {!price ? null : (
              <p className="font-Chillax text-xl text-white">{`$ ${price}`}</p>
            )}
          </div>
          <p className=" text-white opacity-60 font-Chillax text-base ">
            {description}
          </p>
        </div>
      </div>
    </motion.div>
  );
};

export default SingleMenu;
