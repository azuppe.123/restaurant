import React from "react";
import SingleMenu from "./SingleMenu";
import { motion } from "framer-motion";
interface Props {
  rounded: Boolean;
  showSocial: Boolean;
  object: {
    id: string;
    title: string;
    items: {
      id: any;
      name: string;
      vegan: any;
      img: string;
      special: any;
      description: string;
      price: Number | null;
    }[];
  };
}

const variants = {
  open: {
    y: 0,
    opacity: 1,
    transition: {
      
      type: "spring",
      duration: 1,
      delayChildren: 0.5,
      staggerChildren: 0.4,
    },
  },
  closed: {
    y: 20,
    opacity: 0,
    transition: {
      type: "spring",
      duration: 0.3,
    },
  },
};

const itemVariants = {
  open: {
    opacity: 1,
    y: 0,
    transition: { type: "spring", stiffness: 300, damping: 24 },
  },
  closed: { opacity: 0, y: 20, transition: { duration: 0.2 } },
};

const MenuGroup: React.FC<Props> = ({ object, rounded, showSocial }) => {
  const { title, items, id } = object;
  return (
    <motion.div
      variants={variants}
      initial={"closed"}
      animate={"open"}
      className="space-y-8 w-full"
      id={id}
    >
      <p className=" font-bitter-rose text-left text-primary text-[64px] leading-[70%] font-normal">
        {title}
      </p>
      <div>
        <div className="grid grid-cols-1  gap-4">
          {items.map((item, index) => {
            return (
              <motion.div
                variants={itemVariants}
                key={index}
                className="col-span-1"
              >
                <SingleMenu
                  item={item}
                  rounded={rounded}
                  showSocial={showSocial}
                />
              </motion.div>
            );
          })}
        </div>
      </div>
    </motion.div>
  );
};

export default MenuGroup;
