import React, { useState } from "react";

type Props = {};

const dineMenu = [
  {
    name: "Starters",
    href: "#starters",
    slug: "starters",
  },
  {
    name: "Lunch",
    href: "#lunch",
    slug: "lunch",
  },
  {
    name: " Dinner",
    href: "#dinner",
    slug: "dinner",
  },
  {
    name: "Wine",
    href: "#wine",
    slug: "wine",
  },
  {
    name: "Drinks",
    href: "#drinks",
    slug: "drinks",
  },
];

const TopMenu = (props: Props) => {
  const [selected, setSelected] = useState<string>("starters");
  return (
    <nav className="sticky top-0 bg-black z-50 w-full flex justify-center py-6 px-8">
      <ul className="flex gap-12 font-Chillax  text-white opacity-80 text-base">
        {dineMenu.map((item) => {
          const { name, slug, href } = item;
          return (
            <li
              key={slug}
              className={` ${selected === slug ? "text-primary" : ""}`}
            >
              <a href={href} onClick={() => setSelected(slug)}>
                {name}
              </a>
            </li>
          );
        })}
      </ul>
    </nav>
  );
};

export default TopMenu;
