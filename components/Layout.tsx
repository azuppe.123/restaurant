import React from "react";
import Image from "next/image";
import Icon from "./icon/Icon";
import Menu from "./Menu";
import { delay, motion, AnimatePresence, Variants } from "framer-motion";

type Props = {
  img: string;
  title: string;
  subTitle: string;
  hidePT: boolean;
  gap: string;
  children: React.ReactNode;
};

const Layout: React.FC<Props> = ({
  img,
  title,
  subTitle,
  children,
  hidePT,
  gap,
}) => {
  const bgVariants = {
    hidden: {
      opacity: 0,
    },

    visible: {
      opacity: 1,
      y: 0,
      transition: {
        duration: 1,
        type: "linear",
      },
    },
  };
  const extraDiv = {
    hidden: {
      opacity: 1,
    },

    visible: {
      opacity: 0.4,

      transition: {
        duration: 1,
        type: "linear",
      },
    },
  };

  const mainVariants: Variants = {
    hidden: { opacity: 0, x: "-10vw" },
    visible: {
      x: 0,
      opacity: 1,
      transition: {
        delay: 0.5,
        duration: 0.5,
        stiffness: 100,
        type: "spring",
      },
    },
  };

  const subVariants: Variants = {
    hidden: { opacity: 0, x: "10vw" },
    visible: {
      x: 0,
      opacity: 1,
      transition: {
        delay: 1,
        duration: 0.5,
        stiffness: 100,
        type: "spring",
      },
    },
  };

  const menuVariants: Variants = {
    hidden: { opacity: 0, y: "5vh" },
    visible: {
      y: 0,
      opacity: 1,
      transition: {
        delay: 1.5,
        duration: 0.5,
        type: "spring",
      },
    },
  };

  return (
    <div className="flex ">
      <motion.div
        className=" fixed h-[100vh] w-1/2 -z-50 bg-black"
        variants={bgVariants}
        initial={"hidden"}
        animate={"visible"}
      >
        <Image layout="fill" className=" h-[100vh] " src={img} alt="" />
      </motion.div>
      <motion.div
        variants={extraDiv}
        initial={"hidden"}
        animate={"visible"}
        className="fixed h-[100vh] w-1/2 opacity-30 bg-black -z-10"
      ></motion.div>
      <motion.section className="flex flex-col items-center justify-between py-16 h-[100vh] w-1/2 sticky top-0 left-0 ">
        <Icon name="Logo" />
        <motion.div className="">
          <motion.p
            variants={mainVariants}
            initial="hidden"
            animate="visible"
            className=" font-bitter-rose text-center text-primary text-6xl  lg:text-[80px] lg:leading-[90%] font-normal"
          >
            {title}
          </motion.p>
          <motion.p
            variants={subVariants}
            initial="hidden"
            animate="visible"
            className=" text-white font-Chillax-medium text-6xl lg:text-[80px] text-center lg:leading-[90%] "
          >
            {subTitle}
          </motion.p>
        </motion.div>
        <motion.div variants={menuVariants} initial="hidden" animate="visible">
          <Menu />
        </motion.div>
      </motion.section>

      <motion.section
        className={` w-1/2 p-16 ${hidePT ? "pt-0" : null}    bg-black`}
      >
        <motion.div
          variants={bgVariants}
          initial={"hidden"}
          animate={"visible"}
          className={` ${gap} flex flex-col  items-center justify-between`}
        >
          {children}
        </motion.div>
      </motion.section>
    </div>
  );
};

export default Layout;
