import React from "react";
import { atom, selector } from "recoil";

const showModal = atom({
  key: "showModal",
  default: false,
});

export { showModal };
