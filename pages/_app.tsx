import "../styles/globals.css";
import type { AppProps } from "next/app";
import { AnimatePresence } from "framer-motion";
import { RecoilRoot } from "recoil";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <AnimatePresence mode="wait">
      <RecoilRoot>
        <Component {...pageProps} />
      </RecoilRoot>
    </AnimatePresence>
  );
}

export default MyApp;
