import type { NextPage } from "next";
import Icon from "../components/icon/Icon";
import Menu from "../components/Menu";
import OpeningHours from "../components/OpeningHours";
import Image from "next/image";
import MenuGroup from "../components/menucomponents/MenuGroup";
import Footer from "../components/Footer";
import TopMenu from "../components/menucomponents/TopMenu";
import Layout from "../components/Layout";

const items = [
  {
    title: "Starters",
    id: "starters",
    items: [
      {
        id: 1,
        name: "Tomato Toast",
        vegan: true,
        img: "",
        special: true,
        description:
          "Bacon ipsum dolor amet beef corned beef meatloaf chislic flank, ",
        price: 27,
      },
      {
        id: 2,
        name: "Eggplant Toast",
        vegan: true,
        img: "",
        special: "Starter of the Day",
        description:
          "Bacon ipsum dolor amet beef corned beef meatloaf chislic flank, ",
        price: 27,
      },
      {
        id: 3,
        name: "Beef burger",
        vegan: false,
        special: null,
        img: "",
        description:
          "Bacon ipsum dolor amet beef corned beef meatloaf chislic flank, ",
        price: 47,
      },
      {
        id: 4,
        name: "Chicken burger",
        vegan: false,
        special: null,
        img: "",
        description:
          "Bacon ipsum dolor amet beef corned beef meatloaf chislic flank, ",
        price: 17,
      },
    ],
  },
  {
    title: "Breakfast",
    id: "breakfast",
    items: [
      {
        id: 1,
        name: "Tomato Toast",
        vegan: true,
        img: "",
        special: null,
        description:
          "Bacon ipsum dolor amet beef corned beef meatloaf chislic flank, ",
        price: 27,
      },
      {
        id: 2,
        name: "Eggplant Toast",
        vegan: true,
        img: "",
        special: "Starter of the Day",
        description:
          "Bacon ipsum dolor amet beef corned beef meatloaf chislic flank, ",
        price: 27,
      },
      {
        id: 3,
        name: "Beef burger",
        vegan: false,
        special: null,
        img: "",
        description:
          "Bacon ipsum dolor amet beef corned beef meatloaf chislic flank, ",
        price: 47,
      },
      {
        id: 4,
        name: "Chicken burger",
        vegan: false,
        special: null,
        img: "",
        description:
          "Bacon ipsum dolor amet beef corned beef meatloaf chislic flank, ",
        price: 17,
      },
    ],
  },

  {
    title: "Dinner",
    id: "dinner",
    items: [
      {
        id: 1,
        name: "Tomato Toast",
        vegan: true,
        img: "",
        special: null,
        description:
          "Bacon ipsum dolor amet beef corned beef meatloaf chislic flank, ",
        price: 27,
      },
      {
        id: 2,
        name: "Eggplant Toast",
        vegan: true,
        img: "",
        special: "Starter of the Day",
        description:
          "Bacon ipsum dolor amet beef corned beef meatloaf chislic flank, ",
        price: 27,
      },
      {
        id: 3,
        name: "Beef burger",
        vegan: false,
        special: null,
        img: "",
        description:
          "Bacon ipsum dolor amet beef corned beef meatloaf chislic flank, ",
        price: 47,
      },
      {
        id: 4,
        name: "Chicken burger",
        vegan: false,
        special: null,
        img: "",
        description:
          "Bacon ipsum dolor amet beef corned beef meatloaf chislic flank, ",
        price: 17,
      },
    ],
  },
  {
    title: "Wine",
    id: "wine",
    items: [
      {
        id: 1,
        name: "Tomato Toast",
        vegan: true,
        img: "",
        special: null,
        description:
          "Bacon ipsum dolor amet beef corned beef meatloaf chislic flank, ",
        price: 27,
      },
      {
        id: 2,
        name: "Eggplant Toast",
        vegan: true,
        img: "",
        special: "Starter of the Day",
        description:
          "Bacon ipsum dolor amet beef corned beef meatloaf chislic flank, ",
        price: 27,
      },
      {
        id: 3,
        name: "Beef burger",
        vegan: false,
        special: null,
        img: "",
        description:
          "Bacon ipsum dolor amet beef corned beef meatloaf chislic flank, ",
        price: 47,
      },
      {
        id: 4,
        name: "Chicken burger",
        vegan: false,
        special: null,
        img: "",
        description:
          "Bacon ipsum dolor amet beef corned beef meatloaf chislic flank, ",
        price: 17,
      },
    ],
  },
  {
    title: "Drinks",
    id: "drinks",
    items: [
      {
        id: 1,
        name: "Tomato Toast",
        vegan: true,
        img: "",
        special: null,
        description:
          "Bacon ipsum dolor amet beef corned beef meatloaf chislic flank, ",
        price: 27,
      },
      {
        id: 2,
        name: "Eggplant Toast",
        vegan: true,
        img: "",
        special: "Starter of the Day",
        description:
          "Bacon ipsum dolor amet beef corned beef meatloaf chislic flank, ",
        price: 27,
      },
      {
        id: 3,
        name: "Beef burger",
        vegan: true,
        special: true,
        img: "",
        description:
          "Bacon ipsum dolor amet beef corned beef meatloaf chislic flank, ",
        price: 47,
      },
      {
        id: 4,
        name: "Chicken burger",
        vegan: false,
        special: null,
        img: "",
        description:
          "Bacon ipsum dolor amet beef corned beef meatloaf chislic flank, ",
        price: 17,
      },
    ],
  },
];

const menu: NextPage = () => {
  return (
    <div className="flex">
      <Layout gap = "space-y-[96px]" img="/menu.png" title="Check Out" subTitle=" Our Menus" hidePT = {true}>
        <TopMenu />
        {items.map((xl, index) => {
          return (
            <MenuGroup
              key={index}
              object={xl}
              rounded={false}
              showSocial={false}
            />
          );
        })}
        <Footer />
      </Layout>
    </div>
  );
};
export default menu


