import type { NextPage } from "next";
import { showModal } from "../components/atoms";
import { useRecoilValue } from "recoil";
import Main from "../components/Main";

const Home: NextPage = () => {
  const showOpen = useRecoilValue(showModal);
  return (
    <>
      <Main />
    </>
  );
};

export default Home;
