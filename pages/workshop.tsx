import type { NextPage } from "next";
import Icon from "../components/icon/Icon";
import Menu from "../components/Menu";
import OpeningHours from "../components/OpeningHours";
import Image from "next/image";

import WorkShopDetails from "../components/workshop/WorkShopDetails";
import Footer from "../components/Footer";
import Layout from "../components/Layout";

const Workshop: NextPage = () => {
  return (
    <Layout
      img="/workshop.png"
      title=" Shop"
      subTitle="Delicious Breakfast"
      hidePT={false}
    >
      <WorkShopDetails />
      <Footer />
    </Layout>
  );
};

export default Workshop;
