import type { NextPage } from "next";
import ReservationForm from "../components/reservation/ReservationForm";
import Layout from "../components/Layout";

const Reservation: NextPage = () => {
  return (
    <Layout
      img="/reservation.png"
      title="Book a table"
      subTitle="  Reservation"
      hidePT={false}
    >
      <ReservationForm />
    </Layout>
  );
};
export default Reservation
