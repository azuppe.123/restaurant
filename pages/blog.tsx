import type { NextPage } from "next";
import Icon from "../components/icon/Icon";
import Menu from "../components/Menu";
import OpeningHours from "../components/OpeningHours";
import Image from "next/image";
import Footer from "../components/Footer";
import Layout from "../components/Layout";

const items = [
  {
    id: 1,
    name: "Tomato Toast",
    img: "",
    date: "September 19, 2022",
    description:
      "Bacon ipsum dolor amet beef corned beef meatloaf chislic flank, venison leberkas buffalo sirloin turducken meatball. Tail swine biltong kielbasa spare ribs, rump shank shankle.",
  },
  {
    id: 2,
    name: "Eggplant Toast",
    date: "September 19, 2022",
    img: "",
    description:
      "Bacon ipsum dolor amet beef corned beef meatloaf chislic flank, venison leberkas buffalo sirloin turducken meatball. Tail swine biltong kielbasa spare ribs, rump shank shankle.",
  },
  {
    id: 3,
    name: "Beef burger",
    date: "September 19, 2022",
    img: "",
    description:
      "Bacon ipsum dolor amet beef corned beef meatloaf chislic flank, venison leberkas buffalo sirloin turducken meatball. Tail swine biltong kielbasa spare ribs, rump shank shankle.",
  },
  {
    id: 4,
    name: "Chicken burger",
    date: "September 19, 2022",
    img: "",
    description:
      "Bacon ipsum dolor amet beef corned beef meatloaf chislic flank, venison leberkas buffalo sirloin turducken meatball. Tail swine biltong kielbasa spare ribs, rump shank shankle.",
  },
];

const Blog: NextPage = () => {
  return (
    <Layout gap = "space-y-12" img="/blog.png" title="Blog" subTitle="Latest News" hidePT={false}>
      {items.map((xl, index) => {
        const { name, description, date } = xl;
        return (
          <div key={name} className=" flex justify-between p-">
            <div className="flex gap-6 w-full">
              <div className="w-[280px] h-[210px]">
                <Image
                  src={"/pumpkin_soup.webp"}
                  layout="fixed"
                  width={280}
                  height={210}
                  alt="menu"
                  className={`rounded-lg
                        object-cover block`}
                />
              </div>

              <div className="space-y-1">
                <div className="flex justify-between w-full items-center ">
                  <div className="">
                    <p className="font-Chillax text-base text-[#FACE8D]">
                      {date}
                    </p>
                    <h3 className="font-Chillax-medium text-2xl text-white">
                      {name}
                    </h3>
                  </div>
                </div>
                <p className=" text-white opacity-60 font-Chillax text-base">
                  {description}
                </p>
              </div>
            </div>
          </div>
        );
      })}
      <Footer />
    </Layout>
  );
};

export default Blog;
