import React, { useRef, useEffect, useState } from "react";
import { motion, Variants } from "framer-motion";
import Image from "next/image";

const Images = [
  {
    type: "Asian",
    name: "Delicious Breakfast",
    date: "June 16, 2023",
    path: "/restaurant/breakfast.png",
  },
  {
    type: "Breakfast",
    name: "Coffee Time",
    date: "June 16, 2023",
    path: "/restaurant/coffee.png",
  },
  {
    type: "Vegan",
    name: "Vegan Burger",
    date: "January 10, 2023",
    path: "/restaurant/burger.png",
  },
  {
    type: "Italian",
    name: "Salad Style",
    date: "November 19, 2022",
    path: "/restaurant/salad.png",
  },
  {
    type: "Italian",
    name: "Homemade Honey",
    date: "June 16, 2023",
    path: "/restaurant/honey.png",
  },
];

const Restaurant = () => {
  const [width, setWidth] = useState<number>(0);
  const ref = useRef<HTMLDivElement>(null);

  const bgVariants = {
    hidden: {
      opacity: 0,
    },

    visible: {
      opacity: 1,
      y: 0,
      transition: {
        duration: 1,
        type: "linear",
      },
    },
  };
  const extraDiv = {
    hidden: {
      opacity: 1,
    },

    visible: {
      opacity: 0.4,

      transition: {
        duration: 1,
        type: "linear",
      },
    },
  };

  const mainVariants: Variants = {
    hidden: { opacity: 0, y: "10vw" },
    visible: {
      y: 0,
      opacity: 1,
      transition: {
        delay: 0.2,
        duration: 0.5,
        bounce: 0,
        stiffness: 100,
        type: "spring",
      },
    },
  };

  const subVariants: Variants = {
    hidden: { opacity: 0, y: "10vw" },
    visible: {
      y: 0,
      opacity: 1,
      transition: {
        delay: 0.4,
        duration: 0.5,
        bounce: 0,
        stiffness: 100,
        type: "spring",
      },
    },
  };

  const pVariants: Variants = {
    hidden: { opacity: 0, y: "10vw" },
    visible: {
      y: 0,
      opacity: 1,
      transition: {
        delay: 0.8,
        bounce: 0,
        duration: 0.5,
        stiffness: 100,
        type: "spring",
      },
    },
  };

  useEffect(() => {
    setWidth(
      Number(ref.current?.scrollWidth) - ( Number(ref.current?.offsetWidth) )
    );
  }, []);

  return (
    <motion.div
      className="cursor-grab overflow-hidden"
      ref={ref}
      whileTap={{ cursor: "grabbing" }}
    >
      <motion.div
        drag="x"
        dragConstraints={{ right: 0, left: -width }}
        className="w-full flex  "
      >
        {Images.map((item, index) => {
          const { type, name, date, path } = item;
          return (
            <div
              className="flex items-center justify-center min-w-[33.33%]"
              key={index}
            >
              <motion.div
                className=" fixed h-[100vh] w-1/3 -z-50 bg-black"
                variants={bgVariants}
                initial={"hidden"}
                animate={"visible"}
              >
                <Image
                  layout="fill"
                  className=" h-[100vh] "
                  src={path}
                  alt=""
                />
              </motion.div>
              <motion.div
                variants={extraDiv}
                initial={"hidden"}
                animate={"visible"}
                className="fixed h-[100vh] w-1/3 opacity-30 bg-black -z-10"
              ></motion.div>
              <motion.section className="flex flex-col items-center justify-center py-16 h-[100vh] ">
                <motion.div className=" space-y-2 ">
                  <motion.p
                    variants={mainVariants}
                    initial="hidden"
                    animate="visible"
                    className=" font-bitter-rose text-center text-primary text-6xl  lg:text-[64px] lg:leading-[90%] font-normal"
                  >
                    {type}
                  </motion.p>
                  <motion.p
                    variants={subVariants}
                    initial="hidden"
                    animate="visible"
                    className=" text-white font-Chillax-medium text-6xl lg:text-[40px] text-center lg:leading-[90%] "
                  >
                    {name}
                  </motion.p>
                  <motion.p
                    variants={pVariants}
                    initial="hidden"
                    animate="visible"
                    className="font-Chillax text-base text-white opacity-80 text-center"
                  >
                    {date}
                  </motion.p>
                </motion.div>
              </motion.section>
            </div>
          );
        })}
      </motion.div>
    </motion.div>
  );
};

export default Restaurant;
